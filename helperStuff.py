import logging

from matplotlib import pyplot as plt


def createDiagram(numScissors, numRock, numPaper, numSpock, numLizard):
    print('(matplotlib) creating pie chart')
    rpsData = ['Scissor', 'Rock', 'Paper', 'Spock', 'Lizard']
    data = [numScissors, numRock, numPaper, numSpock, numLizard]
    fig = plt.figure(figsize=(10, 7))
    plt.pie(data, labels=rpsData, autopct='%1.1f%%')
    plt.savefig('chart.png')
    return 'chart.png'


def log(txt):
    pass
