from flask import Flask
from flask import abort
from flask import request
from flask import send_file
from flask_cors import CORS

import dbManager
import helperStuff

import logging

logging.basicConfig(filename='log.txt', level=logging.INFO,
                    format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

app = Flask(__name__)
CORS(app)


@app.route('/v1/updateRecord', methods=['POST'])
def updateRecord():
    logging.info('request from ' + request.remote_addr + ' (/v1/updateRecord)')
    argUsername = ""
    argRock = ""
    argScissors = ""
    argPaper = ""
    argSpock = ""
    argLizard = ""

    try:
        argUsername = request.args.get("username")
        argScissors = request.args.get("voteScissors")
        argRock = request.args.get("voteRock")
        argPaper = request.args.get("votePaper")
        argSpock = request.args.get("voteSpock")
        argLizard = request.args.get("voteLizard")
    except:
        abort(400)

    # test args for none
    if argUsername == None:
        abort(400)
    if argScissors == None:
        abort(400)
    if argRock == None:
        abort(400)
    if argPaper == None:
        abort(400)
    if argSpock == None:
        abort(400)
    if argLizard == None:
        abort(400)

    try:
        argScissors = int(argScissors)
        argRock = int(argRock)
        argPaper = int(argPaper)
        argLizard = int(argLizard)
        argSpock = int(argSpock)
    except:
        abort(400)

    if argScissors < 0:
        abort(400)
    if argRock < 0:
        abort(400)
    if argPaper < 0:
        abort(400)
    if argSpock < 0:
        abort(400)
    if argLizard < 0:
        abort(400)

    try:
        dbManager.save(argUsername, argRock, argPaper, argScissors, argSpock, argLizard)
    except:
        abort(500)

    return ""


@app.route('/v1/getGraph', methods=['GET'])
def getGraph():
    logging.info('request from ' + request.remote_addr + ' (/v1/getGraph)')
    try:
        totals = dbManager.totals()
        return send_file(helperStuff.createDiagram(totals["totalScissors"], totals["totalRock"], totals["totalPaper"]
                                                   , totals["totalSpock"], totals["totalLizard"]))
    except:
        abort(404)


@app.route('/v1/getLog', methods=['GET'])
def getLog():
    try:
        return send_file('log.txt')
    except:
        abort(500)


@app.route('/')
def rootAdd():
    logging.info('request from ' + request.remote_addr + ' (/)')
    try:
        return send_file('defaultText.txt')
    except:
        return "It works! Now use the api!"


if __name__ == '__main__':
    logging.info('flask is starting')
    app.run()

""""
python flask: schere stein papier api + als diagramm

api: ergebnisse + username + sinn test (also valid vars, z.b. keine -1 ergebnisse)
Spock, Lizard
"""
