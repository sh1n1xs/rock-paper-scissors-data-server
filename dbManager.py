import logging
import sqlite3


def openConnection():
    try:
        print("(splite) creating database if it doesn't exists already")
        conn = sqlite3.connect('rock-paper-scissors.db')
        print("(sqlite) rock-paper-scissors.db file created and/or connected")
    except:
        print("(sqlite) unable to create database file")

    conn.execute('create table IF NOT EXISTS data('
                 'username TEXT PRIMARY KEY NOT NULL, totalScissors INT, totalRock INT, totalPaper INT,'
                 'totalSpock INT, totalLizard INT'
                 ');')
    return conn


def save(username, rockVote, paperVote, scissorsVote, spockVote, lizardVote):
    conn = openConnection()
    print('(sqlite) inserting data')
    cursor = conn.cursor()
    sql_cmd = "replace INTO data VALUES(?,?,?,?,?,?);"
    data_tuple = (username, scissorsVote, rockVote, paperVote, spockVote, lizardVote)
    cursor.execute(sql_cmd, data_tuple)
    conn.commit()
    conn.close()
    logging.info('updated db for username=' + username)

def totals():
    conn = openConnection()
    print('(sqlite) fetching data')
    logging.info('fetching totals from database')
    sql_cmd = "SELECT SUM(totalScissors) AS scissors, SUM(totalRock) AS rock, SUM(totalPaper) as paper, " \
              "SUM(totalSpock) as spock, SUM(totalLizard) as lizard FROM data;"
    cursor = conn.cursor()
    cursor.execute(sql_cmd)
    records = cursor.fetchmany(1)
    totalScissors = 0
    totalRock = 0
    totalPaper = 0
    totalSpock = 0
    totalLizard = 0
    for row in records:
        totalScissors = row[0]
        totalRock = row[1]
        totalPaper = row[2]
        totalSpock = row[3]
        totalLizard = row[4]
    respondeDict = \
        {
            "totalScissors": totalScissors,
            "totalRock": totalRock,
            "totalPaper": totalPaper,
            "totalSpock": totalSpock,
            "totalLizard": totalLizard
        }
    return respondeDict


# init on startup
openConnection().close()
